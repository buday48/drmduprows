//This package is a fast tool to purify tabulated data from similar rows in specified precision.
//It adheres next concepts:
//
//1. It doesn't transform all the data read form file to its native format (like string s="56" -> int i=56).
//    If cell is not to be compared, it is read as string, stored as string and printed to purified files as string.
//    Such data is 'useless', contrary to 'useful' data which may be compared later after it is read.
//
//2. Since RAM must be allocated to read file anyway, it may be not necessary to clear it every time it is possible.
//    This package doesn't destroy rows as soon as it was found duplicate, it simply marks such rows as 'disabled'.
//    Memory may be restored later if needed or special function to destroy rows as soon as possible may be applied.
// 
//author: Ruslan Budaev, buday48@mail.ru

module drmduprows;

import std.stdio;

/// Classes to represent allocation of cells of each row which are to be effectively compared

enum UsefulCellType : ubyte { integer, fractional, date, time, datetime, duration }		/// Possible types of useful(comparable) cells in table

struct UsefulCell		/// Represents one useful cell to be compared
{
	immutable size_t position;		/// Number of corresponding column in actual row
	immutable UsefulCellType cellType;		/// Type of data stored in cell

	this(size_t position, UsefulCellType cellType)		/// If not specified, integer type is applied
	{
		this.position = position;
		this.cellType = cellType;
	}
}

class Allocation		/// Represents set of UsefulCell classes, i.e. some allocation. Commonly becames a part of a Table class variable when assign it
{
	UsefulCell [] usefulCells;		/// Useful cells with surely increasing numbers of column

	this(UsefulCell [] usefulCells ... )		/// Take into account that columns are indexed from 1(!), not from 0 like in arrays
	{
		import std.exception;
		for(size_t i; i < usefulCells.length; ++i){
			enforce(usefulCells[i].position > 0, "Column number cannot be less than or equal to zero!");
			if(i){
				enforce(usefulCells[i].position > usefulCells[i-1].position, "Column number must increase!");
			}
		}
		this.usefulCells = usefulCells;
	}
}

/// Classes to represent precisions

enum ComparisonType : ubyte { disabled = 0, enabled = 1, match = 2}

struct Delta		/// Elementary precision of one cell within which such cells are considered to be equal(duplicates)
{
	RowUsefulElement delta;		/// Actual precision of specified type

	ComparisonType comparisonType;		/// How this field should be compared
	//bool enabled;		/// Whether this field should be compared

	this(UsefulCellType type, string data, ComparisonType comparisonType = ComparisonType.enabled)
	{
		final switch(type){
			case UsefulCellType.integer :
				delta = new RowInt(data);
				break;
			case UsefulCellType.fractional :
				delta = new RowFract(data);
				break;
			case UsefulCellType.date :
				import std.exception;
				throw new Exception("Date is not allowed type for Delta struct!");
			case UsefulCellType.time :
				import std.exception;
				throw new Exception("Time is not allowed type for Delta struct!");
			case UsefulCellType.datetime :
				import std.exception;
				throw new Exception("DateTime is not allowed type for Delta struct!");
			case UsefulCellType.duration :
				import std.exception;
				throw new Exception("Use Duration to construct Delta struct of Duration type, not string!");
		}
		this.comparisonType = comparisonType;
	}

	import std.datetime;
	this(UsefulCellType type, Duration data, ComparisonType comparisonType = ComparisonType.enabled)
	{
		if( type == UsefulCellType.duration){
				delta = new RowDuration(data);
		} else {
			import std.exception;
			throw new Exception("Use string to construct Delta struct of any type but Duration, not Duration!");
		}
		this.comparisonType = comparisonType;
	}

	this(ComparisonType comparisonType = ComparisonType.enabled)		/// Convenient to use if this field should not be compared
	{
		delta = null;
		this.comparisonType = comparisonType;
	}
}

class DeltaRow		/// Resembling to Row, represents specified precision within which rows are considered as duplicates(equal)
{
	Delta [] deltas;

	this(Delta [] deltas ... )		/// Take into account that columns are indexed from 1(!), not from 0 like in arrays
	{
		this.deltas ~= deltas;
	}
}

/// Set of insructions to read, store, reformat and write data properly
/// Implement your own classes inherited from these three interfaces to work with another file types if necessary

interface InstructionRead		/// Set of functions and parameters to read specified file types
{
	void read( Table table, File file, Allocation allocation);
}

interface InstructionWrite		/// Set of functions and parameters to write to specified file types
{
	void write( Table table, File file);
}

interface InstructionStore		/// Set of functions and parameters to store data read from specified file type and reformat it if necessary
{
	void reformat( Table table);
}

/// Instructions for CSV files are already implemented

class InstructionWriteCSV : InstructionWrite		/// Writes data to file
{
	immutable char delimiter;			/// Delimiter used to print file

	this(char delimiter)
	{
		this.delimiter = delimiter;
	}

	override void write( Table table, File file)
	{
		import std.conv;
		auto initial = to!InstructionStoreCSV(table.instructionStore_);
		import std.exception;
		enforce( initial !is null, "Non-CSV table cannot be printed with InstructionWriteCSV instruction!");
		if( initial.delimiter == delimiter ){
			foreach(row; table.data){
				if( row.enabled){
					foreach(i, part; row.data){
						file.write(part, delimiter);
					}
					file.write('\n');
				}
			}
		} else {
			foreach(row; table.data){
				if( row.enabled){
					foreach(part; row.data){
						char [] part_dup = part.toString().dup;
						foreach( character; part_dup){
							if( character == initial.delimiter ){
								file.write(delimiter);
							} else {
								file.write(character);
							}
						}
						file.write(delimiter);
					}
					file.write('\n');
				}
			}
		}
	}
}

class InstructionStoreCSV : InstructionStore		/// How to store tables read from CSV files: delimiter character must be specified
{
	immutable char delimiter;			/// Delimiter used in file as it is stored in Table class

	this(char delimiter)
	{
		this.delimiter = delimiter;
	}

	bool opEquals( const ref InstructionStoreCSV rhs) const
	{
		return delimiter == rhs.delimiter;
	}

	override void reformat( Table table)		/// Changes delimiter character in stored data
	{
		import std.conv;
		auto initial = to!InstructionStoreCSV(table.instructionStore_);
		import std.exception;
		enforce( initial !is null, "Non-CSV table cannot be printed with InstructionWriteCSV instruction!");
		if( initial.delimiter != delimiter ){
			foreach( row; table.data){
				foreach( element; row.useless){
					auto elementDup = element.data.dup;
					foreach(ref character; elementDup){
						if(character == initial.delimiter){
							character = delimiter;
						}
					}
					element = new RowString( elementDup.idup );
				}
			}
			table.instructionStore_ = this;
		}
		if( table.instructionWrite_ is null){
			table.instructionWrite_ = new InstructionWriteCSV(delimiter);
		} else {
			import std.conv;
			if( to!InstructionWriteCSV(table.instructionWrite_) is null){
				table.instructionWrite_ = new InstructionWriteCSV(delimiter);
			}
		}
	}
}

class InstructionReadCSV : InstructionRead		/// Set of instructions to read CSV file properly
{
	immutable char delimiter;			/// Delimiter used to read file

	this(char delimiter)
	{
		this.delimiter = delimiter;
	}

	override void read( Table table, File file, Allocation allocation)		/// Reads data from file to class Table
	{
		table.data.length = 0;
		import std.string;
		while( !file.eof )
		{
			string row = chomp(file.readln());
			if(row.length){
				table.data ~= parseRowCSV(row, allocation);
			}
		}
		table.instructionRead_ = this;
		table.instructionStore_ = new InstructionStoreCSV(this.delimiter);
		if( table.instructionWrite_ is null){
			table.instructionWrite_ = new InstructionWriteCSV(delimiter);
		} else {
			import std.conv;
			if( to!InstructionWriteCSV(table.instructionWrite_) is null){
				table.instructionWrite_ = new InstructionWriteCSV(delimiter);
			}
		}
	}

	private	Row parseRowCSV(string row, Allocation allocation) const
	{
		auto result = new Row;
		size_t pos;
		string word;
		import std.string;
		foreach(usefulCell; allocation.usefulCells){
			if(usefulCell.position - pos > 1){
				size_t to = indexOf(row, delimiter);
				++ pos;
				while(usefulCell.position - pos > 1){
					to = indexOf(row, delimiter, to + 1);
					++ pos;
				}
				result.useless ~= new RowString(row[0 .. to]);
				row = row[to + 1 .. $];
				result.data ~= result.useless[$ - 1];
			}
			size_t to = indexOf(row, delimiter);
			if(to == -1){
				to = row.length;
			}
			word = row[0 .. to];
			row = row[to + 1 .. $];
			final switch(usefulCell.cellType){
				case UsefulCellType.integer :
					result.useful ~= new RowInt(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.fractional :
					result.useful ~= new RowFract(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.date :
					result.useful ~= new RowDate(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.time :
					result.useful ~= new RowTime(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.datetime :
					result.useful ~= new RowDateTime(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.duration :
					import std.exception;
					throw new Exception("Duration is not allowed type for Row class!");
			}
			++ pos;
		}
		if(row.length){
			result.useless ~= new RowString(row);
			result.data ~= result.useless[$ - 1];
		}
		result.enabled = true;

		return result;
	}
}

class InstructionCSV : InstructionRead, InstructionStore, InstructionWrite		/// Here we fall in some code duplication to implement universal class. Actually there is no need to use InstructionRead/Store/WriteCSV classes, try to use one InstructionCSV class for entire Table instead. Comment InstructionWriteCSV implementation to save compilation time if needed
{
	immutable char delimiter;			/// Delimiter used in file

	this(char delimiter)
	{
		this.delimiter = delimiter;
	}

	bool opEquals( const ref InstructionCSV rhs) const
	{
		return delimiter == rhs.delimiter;
	}

	override void write( Table table, File file)
	{
		import std.conv;
		auto initial = to!InstructionCSV(table.instructionStore_);
		import std.exception;
		enforce( initial !is null, "Non-CSV table cannot be printed with InstructionWriteCSV instruction!");
		if( initial.delimiter == delimiter ){
			foreach(row; table.data){
				if( row.enabled){
					foreach(i, part; row.data){
						file.write(part, delimiter);
					}
					file.write('\n');
				}
			}
		} else {
			foreach(row; table.data){
				if( row.enabled){
					foreach(part; row.data){
						char [] part_dup = part.toString().dup;
						foreach( character; part_dup){
							if( character == initial.delimiter ){
								file.write(delimiter);
							} else {
								file.write(character);
							}
						}
						file.write(delimiter);
					}
					file.write('\n');
				}
			}
		}
	}

	override void reformat( Table table)		/// Changes delimiter character in stored data
	{
		import std.conv;
		auto initial = to!InstructionCSV(table.instructionStore_);
		import std.exception;
		enforce( initial !is null, "Non-CSV table cannot be printed with InstructionWriteCSV instruction!");
		if( initial.delimiter != delimiter ){
			foreach( row; table.data){
				foreach( element; row.useless){
					auto elementDup = element.data.dup;
					foreach(ref character; elementDup){
						if(character == initial.delimiter){
							character = delimiter;
						}
					}
					element = new RowString( elementDup.idup );
				}
			}
			table.instructionStore_ = this;
		}
		if( table.instructionWrite_ is null){
			table.instructionWrite_ = this;		/// Note this distinction from InstructionReadCSV class
		} else {
			import std.conv;
			if( to!InstructionCSV(table.instructionWrite_) is null){
				table.instructionWrite_ = this;		/// Note this distinction from InstructionReadCSV class
			}
		}
	}

	override void read( Table table, File file, Allocation allocation)		/// Reads data from file to class Table
	{
		table.data.length = 0;
		import std.string;
		while( !file.eof )
		{
			string row = chomp(file.readln());
			if(row.length){
				table.data ~= parseRowCSV(row, allocation);
			}
		}
		table.instructionRead_ = this;
		table.instructionStore_ = this;		/// Note this distinction from InstructionReadCSV class
		if( table.instructionWrite_ is null){
			table.instructionWrite_ = this;		/// Note this distinction from InstructionReadCSV class
		} else {
			import std.conv;
			if( to!InstructionCSV(table.instructionWrite_) is null){
				table.instructionWrite_ = this;		/// Note this distinction from InstructionReadCSV class
			}
		}
	}

	private	Row parseRowCSV(string row, Allocation allocation) const
	{
		auto result = new Row;
		size_t pos;
		string word;
		import std.string;
		foreach(usefulCell; allocation.usefulCells){
			if(usefulCell.position - pos > 1){
				size_t to = indexOf(row, delimiter);
				++ pos;
				while(usefulCell.position - pos > 1){
					to = indexOf(row, delimiter, to + 1);
					++ pos;
				}
				result.useless ~= new RowString(row[0 .. to]);
				row = row[to + 1 .. $];
				result.data ~= result.useless[$ - 1];
			}
			size_t to = indexOf(row, delimiter);
			if(to == -1){
				to = row.length;
			}
			word = row[0 .. to];
			row = row[to + 1 .. $];
			final switch(usefulCell.cellType){
				case UsefulCellType.integer :
					result.useful ~= new RowInt(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.fractional :
					result.useful ~= new RowFract(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.date :
					result.useful ~= new RowDate(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.time :
					result.useful ~= new RowTime(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.datetime :
					result.useful ~= new RowDateTime(word);
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.duration :
					import std.exception;
					throw new Exception("Duration is not allowed type for Row class!");
			}
			++ pos;
		}
		if(row.length){
			result.useless ~= new RowString(row);
			result.data ~= result.useless[$ - 1];
		}
		result.enabled = true;

		return result;
	}
}

/// Set of classes represents some parts of table: cells(and another parts of row), rows and a whole table

class RowPart		/// Elements of Row, such as cells as in original file, as well as delimiters, grouped strings of useless data, etc
{
	this(string data){}
	abstract bool isUseful() const @property;		/// Necessary to determine whether this part is useful(used when printing)
}

class RowString : RowPart		/// Useless elements of table such as delimiters or cells that are not being compared, united groups of characters between useful cells, etc
{
	immutable string data;

	static const bool isUseful_ = false;

	override bool isUseful() const @property
	{
		return isUseful_;
	}
	
	override string toString() const 
	{
		return data;
	}
	
	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors
		this.data = data;
	}
}

class RowUsefulElement : RowPart		/// Useful elements of table that are being compared. Variable of this class should never be declared, use subclasses instead(it could be interface at all but implementation of isUseful() function is needed here)
{
	static const bool isUseful_ = true;

	override bool isUseful() const @property
	{
		return isUseful_;
	}

	abstract UsefulCellType type() const @property;		/// Necessary when creating and using Delta classes

	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors
	}
}

class RowInt : RowUsefulElement		/// Represents one cell storing integer number to be compared
{
	immutable int data;

	override string toString() const 
	{
		import std.conv;
		return to!string(data);
	}

	static const UsefulCellType type_ = UsefulCellType.integer;

	override UsefulCellType type() const @property
	{
		return type_;
	}

	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors
		import std.conv;
		this.data = to!int(data);
	}
}

class RowFract : RowUsefulElement		/// Represents one cell storing fractional number to be compared
{
	immutable double data;

	override string toString() const 
	{
		import std.conv;
		return to!string(data);
	}

	static const UsefulCellType type_ = UsefulCellType.fractional;

	override UsefulCellType type() const @property
	{
		return type_;
	}

	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors
		import std.conv;
		this.data = to!double(data);
	}
}

class RowDate : RowUsefulElement		/// Represents one cell storing date to be compared
{
	import std.datetime;
	Date data;
	enum DateFormat {ISO, ISOext, Simple}
	immutable DateFormat dateFormat;
	
	override string toString() const 
	{
		final switch(dateFormat){
			case DateFormat.ISO :
				return data.toISOString();
			case DateFormat.ISOext :
				return data.toISOExtString();
			case DateFormat.Simple :
				return data.toSimpleString();
		}
	}

	static const UsefulCellType type_ = UsefulCellType.date;

	override UsefulCellType type() const @property
	{
		return type_;
	}

	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors

		DateFormat temp;
		try{
			this.data = Date.fromISOString( data);
			temp = DateFormat.ISO;
		} catch(DateTimeException) {
			try{
				this.data = Date.fromISOExtString( data);
				temp = DateFormat.ISOext;
			} catch(DateTimeException) {
				this.data = Date.fromSimpleString( data);
				temp = DateFormat.Simple;
			}
		}
		dateFormat = temp;
	}
}

class RowTime : RowUsefulElement		/// Represents one cell storing time to be compared
{
	import std.datetime;
	TimeOfDay data;
	enum DateFormat {ISO, ISOext,}
	immutable DateFormat dateFormat;

	override string toString() const 
	{
		final switch(dateFormat){
			case DateFormat.ISO :
				return data.toISOString();
			case DateFormat.ISOext :
				return data.toISOExtString();
		}
	}

	static const UsefulCellType type_ = UsefulCellType.time;

	override UsefulCellType type() const @property
	{
		return type_;
	}

	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors

		DateFormat temp;
		try{
			this.data = TimeOfDay.fromISOString( data);
			temp = DateFormat.ISO;
		} catch(DateTimeException) {
			this.data = TimeOfDay.fromISOExtString( data);
			temp = DateFormat.ISOext;
		}
		dateFormat = temp;
	}
}
class RowDateTime : RowUsefulElement		/// Represents one cell storing datetime to be compared
{
	import std.datetime;
	DateTime data;
	enum DateFormat {ISO, ISOext, Simple}
	immutable DateFormat dateFormat;

	override string toString() const 
	{
		final switch(dateFormat){
			case DateFormat.ISO :
				return data.toISOString();
			case DateFormat.ISOext :
				return data.toISOExtString();
			case DateFormat.Simple :
				return data.toSimpleString();
		}
	}

	static const UsefulCellType type_ = UsefulCellType.date;

	override UsefulCellType type() const @property
	{
		return type_;
	}

	this(string data)
	{
		super(data);		/// Needed to avoid compilation errors

		DateFormat temp;
		try{
			this.data = DateTime.fromISOString( data);
			temp = DateFormat.ISO;
		} catch(DateTimeException) {
			try{
				this.data = DateTime.fromISOExtString( data);
				temp = DateFormat.ISOext;
			} catch(DateTimeException) {
				this.data = DateTime.fromSimpleString( data);
				temp = DateFormat.Simple;
			}
		}
		dateFormat = temp;
	}
}

class RowDuration : RowUsefulElement		/// Especial class, used as delta for date/time/datetime comparisons
{
	import std.datetime;
	immutable Duration data;

	override string toString() const 
	{
		import std.conv;
		return to!string(data);
	}

	static const UsefulCellType type_ = UsefulCellType.duration;

	override UsefulCellType type() const @property
	{
		return type_;
	}

	this( Duration data)
	{
		super(string.init);		/// Needed to avoid compilation errors
		this.data = data;
	}
}

class Row		/// An array of useful and useless parts representing existing rows in file with respect to which cells are to be compared or not
{
	RowPart [] data;		/// Used when printing
	RowUsefulElement [] useful;		/// Used when operating on table content
	RowString [] useless;		/// Not used after assignment (and probably may be cleared at all, but I didn't try this)
	bool enabled;		/// Shows if this Row in Table still should be printed(not deleted during any previous purifying from duplicates)

	void check( DeltaRow deltaRow, Row [] samples ...)		/// Assigns true to enabled if row is not duplicate
	{
		foreach( sample; samples){
			bool equal = true;
			foreach( i, delta; deltaRow.deltas){
				if( ( (delta.comparisonType == ComparisonType.enabled) && !comparison(sample.useful[i], this.useful[i], delta.delta) )
						||
					( (delta.comparisonType == ComparisonType.match) && !matchComparison(sample.useful[i], this.useful[i]) ) ){
					equal = false;
					break;
				}
			}
			if( equal){
				enabled = false;
				break;
			}
		}
	}

	private bool comparison( RowUsefulElement cell_1, RowUsefulElement cell_2, RowUsefulElement delta) const
	{
		final switch(cell_1.type){
			case UsefulCellType.integer :
				int c_1 = (cast(RowInt)cell_1).data;
				int c_2 = (cast(RowInt)cell_2).data;
				int d = (cast(RowInt)delta).data;
				int difference = c_1 - c_2;
				if( difference < 0){
					difference = -difference;
				}
				return difference <= d;

			case UsefulCellType.fractional :
				double c_1 = (cast(RowFract)cell_1).data;
				double c_2 = (cast(RowFract)cell_2).data;
				double d = (cast(RowFract)delta).data;
				double difference = c_1 - c_2;
				if( difference < 0){
					difference = -difference;
				}
				return difference <= d;

			case UsefulCellType.date :
				import std.datetime;
				Date c_1 = (cast(RowDate)cell_1).data;
				Date c_2 = (cast(RowDate)cell_2).data;
				Duration d = (cast(RowDuration)delta).data;
				Duration difference = c_1 - c_2;
				if( difference < Duration.init){
					difference = -difference;
				}
				return difference <= d;

			case UsefulCellType.time :
				import std.datetime;
				TimeOfDay c_1 = (cast(RowTime)cell_1).data;
				TimeOfDay c_2 = (cast(RowTime)cell_2).data;
				Duration d = (cast(RowDuration)delta).data;
				Duration difference = c_1 - c_2;
				if( difference < Duration.init){
					difference = -difference;
				}
				return difference <= d;

			case UsefulCellType.datetime :
				import std.datetime;
				DateTime c_1 = (cast(RowDateTime)cell_1).data;
				DateTime c_2 = (cast(RowDateTime)cell_2).data;
				Duration d = (cast(RowDuration)delta).data;
				Duration difference = c_1 - c_2;
				if( difference < Duration.init){
					difference = -difference;
				}
				return difference <= d;

			case UsefulCellType.duration :
				import std.exception;
				throw new Exception("Duration is not allowed type for Row class!");
		}
	}

	private bool matchComparison( RowUsefulElement cell_1, RowUsefulElement cell_2) const
	{
		final switch(cell_1.type){
			case UsefulCellType.integer :
				int c_1 = (cast(RowInt)cell_1).data;
				int c_2 = (cast(RowInt)cell_2).data;
				return c_1 == c_2;

			case UsefulCellType.fractional :
				double c_1 = (cast(RowFract)cell_1).data;
				double c_2 = (cast(RowFract)cell_2).data;
				return c_1 == c_2;

			case UsefulCellType.date :
				import std.datetime;
				Date c_1 = (cast(RowDate)cell_1).data;
				Date c_2 = (cast(RowDate)cell_2).data;
				return c_1 == c_2;

			case UsefulCellType.time :
				import std.datetime;
				TimeOfDay c_1 = (cast(RowTime)cell_1).data;
				TimeOfDay c_2 = (cast(RowTime)cell_2).data;
				return c_1 == c_2;

			case UsefulCellType.datetime :
				import std.datetime;
				DateTime c_1 = (cast(RowDateTime)cell_1).data;
				DateTime c_2 = (cast(RowDateTime)cell_2).data;
				return c_1 == c_2;

			case UsefulCellType.duration :
				import std.exception;
				throw new Exception("Duration is not allowed type for Row class!");
		}
	}
}

class Table		/// Major class in this package: represents tabulated data and set of methods to read, write , purify it from duplicates and more
{
	private Row [] data;		/// Apparent thing: column of rows

	private Allocation allocation_;		/// Determines what columns should be compared or not and its types
	Allocation allocation() @property { return allocation_; }

	private InstructionRead 	instructionRead_;		/// Method to read specified type of file
	InstructionRead instructionRead() @property { return instructionRead_; }
	private InstructionStore	instructionStore_;		/// Method to store data read from specified type of file
	InstructionStore instructionStore() @property { return instructionStore_; }
	private InstructionWrite	instructionWrite_;		/// Method to write to the specified type of file
	InstructionWrite instructionWrite() @property { return instructionWrite_; }
	/// Note this class works properly with plain texts where data fields are separated with any whitespace characters with no instructions

	File log;		/// Where all messages about operation on table are printed to

	void print(File file)		/// Simply prints stored data as-is (adds ' ' character between neighboring useful cells if there is no instruction to write). This function may be used to write some simple-formatted tables to files, such as plain texts(any whitespace is delimiter)
	{
		foreach(row; data){
			if( row.enabled){
				foreach(i, part; row.data){
					if( (i != 0) && (instructionWrite_ is null) && part.isUseful && row.data[i - 1].isUseful ){
						file.write(' ');
					}
					file.write(part);
				}
				file.write('\n');
			}
		}
	}

	void addRow(Row row)		/// Simply adds one Row to existing massive
	{
		if(row !is null){
			data ~= row;
		}
	}

	/// Function to read data from file
	void read( File file, InstructionRead instructionRead = null, Allocation allocation = null, size_t skip = 0)		/// Function to read data from file
	{
		eat( file, skip);
		if( allocation !is null){
			this.allocation_ = allocation;
		}
		if( instructionRead !is null){
			this.instructionRead_ = instructionRead;
		}
		if( this.instructionRead_ !is null){
			if( this.allocation_ !is null ){
				this.instructionRead_.read(this, file, allocation);
			} else {
				this.instructionRead_.read(this, file, new Allocation( [] ) );
			}
		} else {
			if( this.allocation_ !is null ){
				readDefault(file, allocation);
			} else {
				readDefault(file, new Allocation( [] ) );
			}
		}
	}

	this( File file, InstructionRead instructionRead = null, Allocation allocation = null, size_t skip = 0, File log = stdout)
	{
		this( log);
		read( file, instructionRead, allocation, skip);
	}

	this( File log = stdout)
	{
		this.log = log;
	}

	private void eat( File file, size_t skip)
	{
		for( size_t i; i < skip; ++ i){
			string rubbish = file.readln();
		}
	}

	/// Function to reformat data
	void reformat( InstructionStore instructionStore)		/// Function to reformat data
	{
		if( this.instructionStore_ is null){
			log.writeln("Non-formatted table cannot be reformatted!");
		} else {
			if( instructionStore != this.instructionStore_ ){
				instructionStore.reformat(this);
			}
		}
	}

	/// Function to write data to file
	void write( File file, InstructionWrite instructionWrite = null, bool remember = true)		/// Function to write data to file
	{
		if( this.instructionWrite_ is null){
			if( instructionWrite !is null){
				log.writeln("Non-formatted table cannot be printed with any instruction!");
			}
			print(file);
		} else {
			if( instructionWrite !is null){
				if( remember){
					this.instructionWrite_ = instructionWrite;
				}
				instructionWrite.write( this, file);
			} else {
				this.instructionWrite_.write( this, file);
			}
		}
	}

	/// Feature for removing duplicate rows

	void rmduprows( DeltaRow deltaRow, size_t [] sample_numbers ...)
	{
		Row [] samples;
		foreach( sample_number; sample_numbers){
			samples ~= data[sample_number];
		}
		foreach( i, element; data){
			bool is_sample = false;
			foreach( sample_number; sample_numbers){
				if( element.enabled && sample_number == i){
					is_sample = true;
					break;
				}
			}
			if( is_sample){
				continue;
			}
			element.check(deltaRow, samples);
		}
	}

	void rmduprowsIrrevocably( DeltaRow deltaRow, size_t [] sample_numbers ...)
	{
		Row [] samples;
		foreach( sample_number; sample_numbers){
			samples ~= data[sample_number];
		}
		foreach( i, element; data){
			bool is_sample = false;
			foreach( sample_number; sample_numbers){
				if( element.enabled && sample_number == i){
					is_sample = true;
					break;
				}
			}
			if( is_sample){
				continue;
			}
			element.check(deltaRow, samples);
		}
		destroyDuplicates();
	}

	void destroyDuplicates()		/// Some features to work with allocated memory
	{
		for( size_t i; i < data.length; ++ i){
			if( !data[i].enabled){
				destroy( data[i]);
				data = data[0 .. i] ~ data[i + 1 .. $];
				-- i;
			}
		}
	}

	void retrieveAll()		/// Retrieving disabled rows to enabled state
	{
		foreach( element; data){
			element.enabled = true;
		}
	}

private:
	void readDefault( File file, Allocation allocation)
	{
		import std.string;
		import std.array;
		import std.conv;
		while( !file.eof() ){
			string row = chomp( file.readln() );
			if( row.length ){
				data ~= parseRowDefault(row, allocation);
			}
		}
	}

	private Row parseRowDefault(string row, Allocation allocation)
	{
		auto result = new Row;
		import std.array;
		string [] rowar = split(row); 
		size_t pos;
		foreach(usefulCell; allocation.usefulCells){
			if(usefulCell.position - pos > 1){
				string temp;
				if( pos ){
					temp = " ";
				}
				while(usefulCell.position - pos > 1){
					temp ~= (rowar[pos] ~ ' ').idup;
					++ pos;
				}
				result.useless ~= new RowString( temp );
				result.data ~= result.useless[$ - 1];
			}
			final switch(usefulCell.cellType){
				case UsefulCellType.integer :
					result.useful ~= new RowInt( rowar[pos].idup );
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.fractional :
					result.useful ~= new RowFract( rowar[pos].idup );
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.date :
					result.useful ~= new RowDate( rowar[pos].idup );
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.time :
					result.useful ~= new RowTime( rowar[pos].idup );
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.datetime :
					result.useful ~= new RowDateTime( rowar[pos].idup );
					result.data ~= result.useful[$ - 1];
					break;
				case UsefulCellType.duration :
					import std.exception;
					throw new Exception("Duration type is not allowed type for Row class!");
			}
			++ pos;
		}
		if(pos < rowar.length){
			string tail;
			foreach( elem; rowar[pos .. $] ){
				tail ~= ' ';
				tail ~= elem;
				tail ~= ' ';
			}
			result.useless ~= new RowString( tail );
			result.data ~= result.useless[$ - 1];
		}
		result.enabled = true;

		return result;
	}
}
