import std.stdio;
import drmduprows;		// Adds library

void main()
{
	auto table =			// For first create and read new table from file
		new Table( File("book.csv", "r"),			//	File to read from
				   new InstructionCSV(';'),				// Read a csv table where ';' character is delimiter
				   new Allocation( UsefulCell(1, UsefulCellType.integer),			// Read 1st column as integer
								   UsefulCell(2, UsefulCellType.fractional),			// 2nd column as fractional
								   UsefulCell(5, UsefulCellType.time) ), 			// And 5th column as time of day
				   1 );				// Skipping one string at the beginning

	import std.datetime;
	table.rmduprows( new DeltaRow( Delta( ComparisonType.match ),			// Delete exact duplicates...
								   Delta( ComparisonType.match ),
								   Delta( ComparisonType.match ) ), 0 );		// ...of first row for first...

	table.write( File("book_out1.csv", "w") );				// ...and write it to new file.

	table.rmduprows( new DeltaRow( Delta(),
								   Delta(),
								   Delta( UsefulCellType.duration, dur!"seconds"(30)) ), 0 );		// Delete data in 30 seconds range

	table.write( File("book_out2.csv", "w") );				// Check new output

	table.rmduprows( new DeltaRow( Delta( UsefulCellType.integer, "1" ),		// Let's define more complicated precision
								   Delta( UsefulCellType.fractional, "0.05" ),
								   Delta() ), 0 );

	table.write( File("book_out3.csv", "w") );				// Check it

}
